(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("48042425e84cd92184837e01d0b4fe9f912d875c43021c3bcb7eeb51f1be5710" "b5fd9c7429d52190235f2383e47d340d7ff769f141cd8f9e7a4629a81abc6b19" default))
 '(delete-selection-mode nil)
 '(warning-suppress-types '((comp))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
;; Somewhere in your .emacs file

;; elfeed
(setq elfeed-feeds
      '(
        "https://planet.fsci.in/atom.xml"
        "https://sizeof.cat/index.xml"
        "https://blog.lispy.tech/feed.xml"
        "https://drewdevault.com/blog/index.xml"
        "https://arunmani.in/rss"
        "https://phaazon.net/blog/feed"
        "https://ravidwivedi.in/posts/index.xml"
        "https://dhruvin.dev/blog/index.xml"
        "https://fsci.in/blog/index.xml"
        ))
