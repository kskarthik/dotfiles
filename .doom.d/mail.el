;;; ../dotfiles/.doom.d/mail.el -*- lexical-binding: t; -*-
(setq
 ;; The mail URL, specifying a remote mail account
 ;; (Omit this to read from /var/mail/user)
 rmail-primary-inbox-list '("imap://kskarthik@disroot.org")
 rmail-movemail-variant-in-use 'mailutils
 rmail-remote-password-required t
 send-mail-function 'smtpmail-send-it       ; Send mail via SMTP
 rmail-preserve-inbox 1                     ; Don't delete mail from server
 rmail-mail-new-frame 1                     ; Compose in a full frame
 rmail-delete-after-output 1                ; Delete original mail after copying
 rmail-mime-prefer-html nil                 ; Prefer plaintext when possible
 rmail-file-name   "~/mail/inbox"           ; The path to our inbox file
 rmail-secondary-file-directory "~/mail"    ; The path to our other mbox files
 message-default-headers "Fcc: ~/mail/sent" ; Copy sent mail to the "sent" file
 user-full-name    "Sai Karthik"                  ; Our full name
 user-mail-address "kskarthik@disroot.org"         ; Our return address
)
