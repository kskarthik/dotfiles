;;; blog.el -*- lexical-binding: t; -*-

;; org-static-blog setup

;; Credits: http://ergoemacs.org/emacs/elisp_read_file_content.html
(defun get-string-from-file (path)
  "Return filePath's file content."
  (with-temp-buffer
    (insert-file-contents path)
    (buffer-string)))

(setq org-static-blog-publish-title "Sai Karthik")
(setq org-static-blog-publish-url "/")
(setq org-static-blog-publish-directory "~/blog/public/")
(setq org-static-blog-posts-directory "~/blog/posts/")
(setq org-static-blog-drafts-directory "~/blog/drafts/")
(setq org-static-blog-enable-tags t)
(setq org-export-with-toc nil)
(setq org-export-with-section-numbers nil)
;; This header is inserted into the <head> section of every page:
(setq org-static-blog-page-header (get-string-from-file "~/blog/partials/header.html"))
;; This preamble is inserted at the beginning of the <body> of every page:
;;   This particular HTML creates a <div> with a simple linked headline
(setq org-static-blog-page-preamble (get-string-from-file "~/blog/partials/preamble.html"))


;; This postamble is inserted at the end of the <body> of every page:
;;   This particular HTML creates a <div> with a link to the archive page
;;   and a licensing stub.
(setq org-static-blog-page-postamble (get-string-from-file "~/blog/partials/postamble.html"))

;; This HTML code is inserted into the index page between the preamble and
;;   the blog posts
(setq org-static-blog-index-front-matter
"<div class='title is-1'> Recent posts</div>\n")
