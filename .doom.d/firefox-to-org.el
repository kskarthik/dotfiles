;;; ../dotfiles/.doom.d/firefox-to-org.el -*- lexical-binding: t; -*-

;; Credits: http://ergoemacs.org/emacs/elisp_read_file_content.html
(defun string-from-file (path)
  "Return filePath's file content."
  (with-temp-buffer
    (insert-file-contents path)
    (buffer-string)))

(setq
 ;; read file
 f2o-file-path (read-file-name "Select Firefox Bookmark file: " "~/")

 ;; convert json to hash tables
 bookmarks (json-parse-string (string-from-file f2o-file-path) :object-type 'alist)
 ;; access the children array
 ac (gethash "children" bookmarks)

 ;; org file name
 f2o-dest-filename "firefox-bookmarks"

 ;; Destination directory of org file
 f2o-dest-dir 'org-directory
 )

(defun f2o-convert-org ()
  "Loop through the json file & convert it to org a document"
  (interactive)
  ;; (setq tmp-file (make-temp-file (concat f2o-dest-dir f2o-dest-filename) nil ".org"))
  ;; loop over hash table
  (with-temp-buffer
    (maphash
     (lambda (key val)
       (princ-list key)
       )
     ac)
    (buffer-string)
    )
  )
  (f2o-convert-org)
