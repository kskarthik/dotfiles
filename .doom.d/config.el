;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (load! "blog" doom-private-dir)
(load! "my-funcs" doom-private-dir)
(setq doom-font (font-spec :family "JetBrains Mono" :size 19 :spacing 2 :weight 'semi-light)
      doom-variable-pitch-font (font-spec :family "JetBrains Mono" :size 19))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'catppuccin)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/my/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq global-hl-line-mode t)

;;  Show battery info
(display-battery-mode)

;; LSP config
(setq lsp-ui-sideline-enable nil)
(setq lsp-ui-doc-enable t)
(setq lsp-ui-doc-show-with-cursor t)
(setq +format-with-lsp nil)

(pixel-scroll-precision-mode)

;; projectile
(setq projectile-project-search-path '(("~/my" . 1)))


;; HOOKS
;; activate lsp in web mode
(add-hook 'web-mode-hook #'lsp-deferred)
(add-hook 'elfeed-search-mode-hook 'elfeed-update)
(add-hook 'projectile-find-file 'projectile-purge-dir-from-cache "node_modules")
(add-hook 'evil-insert-state-exit-hook 'my/save-buffer 'my/format-html)
(add-hook! 'pyvenv-post-activate-hooks 'lsp-restart-workspace)
(add-to-list 'company-backends 'company-emoji)

;; org mode eval
(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . nil)
   (python . t)))

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
(define-key local-function-key-map (kbd "<Launch1>") (kbd ">"))
