#!/usr/bin/python3
import subprocess as s
import json


def swap():
    r = s.run(["swaymsg", "-t", "get_outputs", "-r"], capture_output=True)

    monitors = json.loads(r.stdout)

    if len(monitors) > 1:
        for i in monitors:
            if i["name"] == "HDMI-A-1":
                s.run(["swaymsg", "output", i["name"], "enable"])
                s.run(["swaymsg", "output", "LVDS-1", "disable"])
                s.run(["swaymsg", "output", "LVDS-1", "disable"])
                return

    # enable native monitor if no ext monitor is attached
    else:
        s.run(["swaymsg", "output", monitors[0]["name"], "enable"])
        # set color temp
        s.run(["gammastep", "-O", "5000K"])


swap()

