{
  "position": "top", // Waybar position (top|bottom|left|right)
                     // Choose the order of the modules
    "modules-left": ["sway/workspaces", "temperature", "sway/language", "user", "custom/media"],
    "modules-center": ["clock"],
    "modules-right": ["idle_inhibitor", "pulseaudio", "network", "cpu", "memory", "backlight", "battery", "custom/poweroff","tray"],

    // Modules configuration
    //"sway/workspaces": {
    //  "disable-scroll": true,
    //  "all-outputs": true,
    //  "format": "{name}: {icon}",
    //  "format-icons": {
    //    "1": "",
    //    "2": "",
    //    "3": "",
    //    "4": "",
    //    "5": "",
    //    "urgent": "",
    //    "focused": "",
    // }
    //},
    "sway/mode": {
      "format": "<span style=\"italic\">{}</span>",
      "icon": true
    },
    "mpd": {
      "format": "{stateIcon} {consumeIcon}{randomIcon}{repeatIcon}{singleIcon}{artist} - {album} - {title} ({elapsedTime:%M:%S}/{totalTime:%M:%S}) ⸨{songPosition}|{queueLength}⸩ ",
      "format-disconnected": "Disconnected ",
      "format-stopped": "{consumeIcon}{randomIcon}{repeatIcon}{singleIcon}Stopped ",
      "unknown-tag": "N/A",
      "interval": 2,
      "consume-icons": {
        "on": " "
      },
      "random-icons": {
        "off": "<span color=\"#f53c3c\"></span> ",
        "on": " "
      },
      "repeat-icons": {
        "on": " "
      },
      "single-icons": {
        "on": "1 "
      },
      "state-icons": {
        "paused": "",
        "playing": ""
      },
      "tooltip-format": "MPD (connected)",
      "tooltip-format-disconnected": "MPD (disconnected)"
    },
    "idle_inhibitor": {
      "format": "{icon}",
      "format-icons": {
        "activated": "",
        "deactivated": ""
      }
    },
    "tray": {
      "icon-size": 21,
      "spacing": 10
    },
    "clock": {
      // "timezone": "America/New_York",
      "tooltip": true,
      "tooltip-format": "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>",
      // "format-alt": "{:%Y-%m-%d}",
      "format": "{:%H:%M | %d-%m-%Y}",
    },
    "cpu": {
      "format": "{usage}% ",
      "tooltip": true,
      "interval": 2
    },
    "memory": {
      "format": "{used}GB "
    },
    "temperature": {
      // "thermal-zone": 2,
      "hwmon-path": "/sys/class/hwmon/hwmon2/temp1_input",
      "critical-threshold": 80,
      "format-critical": "{temperatureC}°C {icon}",
      "format": "{temperatureC}°C {icon}",
      "format-icons": ["", "", ""]
    },
    "backlight": {
      /* "device": "acpi_video1", */
      "format": "{percent}% {icon}",
      "format-icons": ["", ""],
      "on-scroll-up": "brightnessctl s +1%",
      "on-scroll-down": "brightnessctl s 1%-"
    },
    "battery": {
      "states": {
        "good": 90,
        "warning": 40,
        "critical": 20
      },
      "format": "{capacity}% {icon}",
      "format-time": "({H}:{m})",
      "format-charging": "{capacity}% ",
      "format-plugged": "{capacity}% ",
      "format-alt": "{time} {icon}",
      // "format-good": "", // An empty format will hide the module
      // "format-full": "",
      "format-icons": ["", "", "", "", ""]
    },
    "battery#bat2": {
      "bat": "BAT2"
    },
    "network": {
      // "interface": "wlp2*", // (Optional) To force the use of this interface
      "format-wifi": "{essid} ({signalStrength}%) ",
      "format-ethernet": "{ifname}: {ipaddr}/{cidr} ",
      "format-linked": "{ifname} (No IP) ",
      "format-disconnected": "Disconnected ⚠",
      //"format-alt": "{ifname}: {ipaddr}/{cidr}",
      "on-click": "alacritty -e nmtui"
    },
    "pulseaudio": {
      // "scroll-step": 1, // %, can be a float
      "format": "{volume}% {icon} {format_source}",
      "format-bluetooth": "{volume}% {icon} {format_source}",
      "format-bluetooth-muted": " {icon} {format_source}",
      "format-muted": "🔕 {format_source}",
      "format-source": "{volume}% ",
      "format-source-muted": "",
      "format-icons": {
        "headphone": "",
        "hands-free": "",
        "headset": "",
        "phone": "",
        "portable": "",
        "car": "",
        "default": ["", "", ""]
      },
      "on-click": "foot pulsemixer"
    },
    "custom/poweroff": {
      "format": "⏻",
      "on-hover": "Power off",
      "on-click": "systemctl poweroff",

    },
    "custom/media": {
      "format": "{icon} {}",
      "return-type": "json",
      "max-length": 40,
      "format-icons": {
        "spotify": "",
        "default": "🎜"
      },
      "escape": true,
      "exec": "$HOME/.config/waybar/mediaplayer.py 2> /dev/null" // Script in resources folder
                                                                 // "exec": "$HOME/.config/waybar/mediaplayer.py --player spotify 2> /dev/null" // Filter player based on name
    },
    "sway/language": {
      "format": "{flag} {short}-{shortDescription}",
    },
    "user": {
      "format": "Uptime: {work_H}:{work_M}",
      "interval": 60,
    }
}
