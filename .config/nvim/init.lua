vim.cmd([[
set notimeout
set encoding=utf-8
set number
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set mouse=a
let g:neovide_cursor_vfx_mode = "torpedo"
set guifont=JetBrainsMono:h18
" disable window specific status lines
set laststatus=3

" augroup vimrc-incsearch-highlight
"   autocmd!
"   autocmd CmdlineEnter /,\? :set hlsearch
"   autocmd CmdlineLeave /,\? :set nohlsearch
" augroup END
set title
]])

vim.o.cursorline = true

-- require('packer-plugins')
require("keymaps")

local opt = vim.opt

opt.termguicolors = true

-- paste from clipboard
opt.clipboard = "unnamedplus"

-- bootstrap lazyvim if not already installed
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable", -- latest stable release
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)
-- load lazyvim
require("lazy").setup("plugins")
