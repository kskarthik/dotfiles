-- [[
-- plugins
--]]

--- global leader key is space bar
vim.g.mapleader = " "

map = vim.keymap.set
-- reload config
map("n", "<leader>r", ":source %<CR>", {})

-- open nvim config dir for editing
map("n", "<leader>c", ":e ~/.config/nvim<CR>", {})

-- Telescope file search
map("n", "<leader><leader>", ":Telescope find_files<CR>", {})

-- map('n', '<leader>g', function () require('neogit').open() end, {})

-- Toggle file explorer
map("n", "<leader>e", ":NvimTreeToggle <CR>", {})

-- sync packages
map("n", "<leader>u", ":Lazy sync<CR>", {})

-- Kill buffer without losing layout
-- Ref: https://stackoverflow.com/a/8585343
map("n", "<Leader>k", ":bprevious|split|bnext|bdelete<CR>")

--[[
-- Window management
--]]
-- Use alt+{h,j,k,l} to switch windows in normal mode
map("n", "<A-h>", "<C-w>h")
map("n", "<A-j>", "<C-w>j")
map("n", "<A-k>", "<C-w>k")
map("n", "<A-l>", "<C-w>l")
-- close window
map("n", "<A-c>", "<C-w>q")
-- Toggle running windows
map("n", "<A-w>", "<C-w>w")
-- Horizontal split
map("n", "<A-s>", "<C-w>s")
-- Vertical Split
map("n", "<A-v>", "<C-w>v")
-- save file
map("n", "<C-s>", ":w<CR>")

vim.keymap.set("n", "<leader><Left>", ":bp<CR>", {})
vim.keymap.set("n", "<leader><Right>", ":bn<CR>", {})

--[[ vim.api.nvim_create_autocmd('InsertLeave', {
	callback = function()
		vim.cmd("write")
	end
}) ]]
