-- https://github.com/NeogitOrg/neogit
return {
	"NeogitOrg/neogit",
	dependencies = {
		"nvim-lua/plenary.nvim",       -- required
		"nvim-telescope/telescope.nvim", -- optional
		"sindrets/diffview.nvim",      -- optional
		"ibhagwan/fzf-lua",            -- optional
	},
	-- tag = "v0.0.1",
	config = function()
		require("neogit").setup()
		map("n", "<leader>g", ":Neogit<CR>", {})
	end,
}
