local function setup_null_ls()
	-- null-ls setup
	local null_ls = require("null-ls")
	null_ls.setup({
		sources = {
			null_ls.builtins.formatting.prettier,
			null_ls.builtins.formatting.ruff,
			null_ls.builtins.formatting.rustfmt,
			null_ls.builtins.formatting.stylua,
			null_ls.builtins.formatting.gofmt,
		},
		-- you can reuse a shared lspconfig on_attach callback here
		on_attach = function(client, bufnr)
			if client.supports_method("textDocument/formatting") then
				vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
				vim.api.nvim_create_autocmd("BufWritePre", {
					group = augroup,
					buffer = bufnr,
					callback = function()
						-- on 0.8, you should use vim.lsp.buf.format({ bufnr = bufnr }) instead
						-- on later neovim version, you should use vim.lsp.buf.format({ async = false }) instead
						vim.lsp.buf.format({ async = false })
					end,
				})
			end
		end,
	})
end

local function setup_lsp_zero()
	local lsp = require("lsp-zero").preset({
		name = "recommended",
		set_lsp_keymaps = true,
		manage_nvim_cmp = {
			set_sources = "recommended",
		},
		suggest_lsp_servers = false,
		configure_diagnostics = true,
	})
	vim.diagnostic.config({
		virtual_text = true,
	})
	-- lsp.on_attach(function(client, bufnr)
	-- 	lsp.default_keymaps({buffer = bufnr})
	-- end)
	lsp.setup()

	-- completion
	local cmp = require("cmp")
	cmp.setup({
		preselect = "item",
		completion = {
			completeopt = "menu,menuone,noinsert",
		},
		mapping = {
			["<CR>"] = cmp.mapping.confirm({ select = false }),
		},
	})
	local cmp_action = require("lsp-zero").cmp_action()

	require("luasnip.loaders.from_vscode").lazy_load()

	cmp.setup({
		sources = {
			{ name = "path" },
			{ name = "nvim_lsp" },
			{ name = "buffer",  keyword_length = 3 },
			{ name = "luasnip", keyword_length = 2 },
			{ name = 'nvim_lua' },
			{ name = 'emoji' },
		},
		mapping = {
			["<C-f>"] = cmp_action.luasnip_jump_forward(),
			["<C-b>"] = cmp_action.luasnip_jump_backward(),
		},
	})
	-- lsp.format_on_save({
	-- 	-- format_opts = {
	-- 	-- 	timeout_ms = 10000,
	-- 	-- },
	-- 	servers = { ["null-ls"] = { "go", "javascript", "typescript", "lua", "vue", "python", "markdown" } },
	-- })
	setup_null_ls()
end

return {
	{
		"VonHeikemen/lsp-zero.nvim",
		branch = "v2.x",
		lazy = false,
		dependencies = {
			-- LSP Support
			{ "neovim/nvim-lspconfig" }, -- Required
			-- Autocompletion
			{ "hrsh7th/nvim-cmp" },   -- Required
			{ "hrsh7th/cmp-nvim-lsp" }, -- Required
			{ "hrsh7th/cmp-buffer" },
			{ "hrsh7th/cmp-path" },
			{ "saadparwaiz1/cmp_luasnip" },
			{ "hrsh7th/cmp-nvim-lua" },
			-- Snippets
			{ "L3MON4D3/LuaSnip" },
			{ "rafamadriz/friendly-snippets" },
			-- null-ls (deprecated)
			-- { "jose-elias-alvarez/null-ls.nvim" },
			-- null-ls fork https://github.com/nvimtools/none-ls.nvim
			{ "nvimtools/none-ls.nvim" },
			-- mason
			{
				"williamboman/mason.nvim",
				build = ":MasonUpdate",               -- :MasonUpdate updates registry contents
				dependencies = {
					{ "williamboman/mason-lspconfig.nvim" }, -- Optional
				},
				config = function()
					require("mason").setup()
				end,
			},
		},
		config = function()
			setup_lsp_zero()
		end,
	},
}
