-- Terminal
return {
	"akinsho/toggleterm.nvim",
	config = function()
		require("toggleterm").setup({
			open_mapping = [[<C-t>]],
			insert_mappings = true,
			terminal_mappings = true,
			direction = "float",
			size = 10,
			persist_size = true,
		})
	end,
}
