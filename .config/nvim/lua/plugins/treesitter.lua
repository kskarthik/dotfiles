return {
	-- Treesitter configurations and abstraction layer
	{
		"nvim-treesitter/nvim-treesitter",
		lazy = false,
		dependencies = {
			{ "nvim-treesitter/nvim-treesitter-refactor" },
			{ "nvim-treesitter/nvim-treesitter-textobjects" },
			{ "windwp/nvim-ts-autotag" },
			{ "JoosepAlviste/nvim-ts-context-commentstring" },
		},
		config = function()
			vim.g.skip_ts_context_commentstring_module = true
			require("nvim-treesitter.configs").setup({
				auto_tag = { enable = true },
				ts_context_commentstring = { enable = true },
				ensure_installed = { "python", "javascript", "vue", "lua" },
				-- List of parsers to ignore installing (for "all")
				ignore_install = { "markdown" },
				highlight = {
					enable = true,
				},
				incremental_selection = {
					enable = true,
				},
				indent = {
					enable = true,
				},
				refactor = {
					highlight_definitions = {
						enable = true,
						clear_on_cursor_move = true,
					},
					navigation = {
						enable = true,
						keymaps = {
							goto_definition_lsp_fallback = "gd",
							list_definitions = "gD",
							list_definitions_toc = "gO",
							goto_next_usage = "<A-n>",
							goto_previous_usage = "<A-S-n>",
						},
					},
				},
				textobjects = {
					select = {
						enable = true,
						lookahead = true,
						keymaps = {
							["ac"] = "@class.outer",
							["ic"] = "@class.inner",
							["af"] = "@function.outer",
							["if"] = "@function.inner",
						},
					},
					lsp_interop = {
						enable = true,
						border = "none",
						peek_definition_code = {
							["<Leader>dF"] = "@class.outer",
							["<Leader>df"] = "@function.outer",
						},
					},
				},
			})
		end,
	},
}
