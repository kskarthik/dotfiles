-- auto save
return {
	"Pocco81/auto-save.nvim",
	config = function()
		require("auto-save").setup({
			trigger_events = { "InsertLeave" }, -- vim events that trigger auto-save. See :h events
		})
	end,
}
