-- Statusline
return {
	"nvim-lualine/lualine.nvim",
	dependencies = { "kyazdani42/nvim-web-devicons", opt = true },
	config = function()
		require("lualine").setup({
			theme = "auto",
			sections = {
				lualine_c = { { "filename", path = 1 } },
				lualine_x = { "filetype", "filesize", "encoding" },
			},
		})
	end,
}
