-- Telescope (Fuzzy finder & more ...)
-- All telescope dependant plugins are also
-- listed in this file
return {
	{
		"nvim-telescope/telescope.nvim",
		lazy = true,
		dependencies = {
			{ "nvim-lua/plenary.nvim" },
			-- { "nvim-telescope/telescope-file-browser.nvim" }, -- file browser
		},
		config = function()
			vim.keymap.set("n", "<leader>lg", ":Telescope live_grep<CR>")
			vim.keymap.set("n", "<leader>lf", ":Telescope lsp_references<CR>")
			vim.keymap.set("n", "<leader>h", ":Telescope commands<CR>")
			-- require("telescope").load_extension("file_browser")
			require("telescope").setup({
				-- lsp stuff
				defaults = {
					preview = {
						filesize_limit = 3,
					},
				},
				pickers = {
					find_files = {
						-- show hidden files
						hidden = true,
					},
				},
				mappings = {
					i = {
						-- map actions.which_key to <C-h> (default: <C-/>)
						-- actions.which_key shows the mappings for your picker,
						-- e.g. git_{create, delete, ...}_branch for the git_branches picker
						["<C-h>"] = "which_key",
					},
				},
				extensions = {
					--[[ project = {
						base_dirs = {
							{ path = "~/my", max_depth = 3 },
						},
						hidden_files = true, -- default: false
						theme = "dropdown",
						sync_with_nvim_tree = true,
					}, ]]
					-- file_browser = {
					-- 	theme = "ivy",
					-- 	-- disables netrw and use telescope-file-browser in its place
					-- 	hijack_netrw = true,
					-- 	hidden = true,
					-- 	collapse_dirs = true,
					-- },
				},
			})
		end,
	},
}
