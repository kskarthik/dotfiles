-- Indent guides
return {
	"lukas-reineke/indent-blankline.nvim",
	main = "ibl",
	config = function()
		vim.opt.list = true
		-- vim.g.indent_blankline_char = '|'
		-- vim.opt.listchars:append "space:⋅"
		-- vim.opt.listchars:append "eol:↴"
		require("ibl").setup()
	end,
}
