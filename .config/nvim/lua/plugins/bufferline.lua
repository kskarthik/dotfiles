-- bufferline
return {
	"akinsho/bufferline.nvim",
	dependencies = "kyazdani42/nvim-web-devicons",
	config = function()
		require("bufferline").setup({ options = { seperator_style = "padded_slant" } })
	end,
}
