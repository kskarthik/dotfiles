-- WhichKey
return {
	"folke/which-key.nvim",
	config = function()
		vim.cmd([[
			set timeout
			set timeoutlen=500
			]])
		require("which-key").setup()
	end,
}
