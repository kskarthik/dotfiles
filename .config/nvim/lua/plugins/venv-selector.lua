return {
	"linux-cultist/venv-selector.nvim",
	dependencies = { "neovim/nvim-lspconfig", "nvim-telescope/telescope.nvim" },
	config = function()
		require("venv-selector").setup({
			pipenv_path = "/home/sk/virtualenvs/",
		})
	end,
	branch = "regexp",
	event = "VeryLazy", -- Optional: needed only if you want to type `:VenvSelect` without a keymapping
}
