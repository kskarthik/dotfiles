;;(add-to-list 'load-path "/path/to/installed-package-repo") ;; consider changing this in future
;; (load "my-funcs.el")
;; ( require 'my-funcs)
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(eval-and-compile
  (setq use-package-always-ensure t
	use-package-expand-minimally t))
(package-initialize)

(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

(global-display-line-numbers-mode 1)
(global-visual-line-mode t)

(setq make-backup-files nil)

(set-frame-font "JetbrainsMono 14" nil t)
;; (set-face-attribute 'default nil :height 160)

(setq inhibit-startup-screen t)

(setq warning-minimum-level :error)

(use-package catppuccin-theme
  :ensure t)
(load-theme 'catppuccin t nil)

(use-package which-key :ensure t)
(require 'which-key)
(which-key-mode)

(defun async-upgrade-packages ()
  "Update packages to latest version without blocking Emacs."
  (interactive)
  (message "Upgrading packages in the background 🚀")
  (async-start (lambda ()
		 (package-refresh-contents)
		 (package-upgrade-all))
	       (lambda (result)(message result))))

(use-package evil
  :ensure t
  :init      ;; tweak evil's configuration before loading it
  ;;(setq evil-want-keybinding nil)
  (setq evil-vsplit-window-right t)
  (setq evil-split-window-below t)
  (evil-mode))

(use-package evil-leader
  :ensure t
  :config
  (global-evil-leader-mode t)
  (evil-leader/set-leader "<SPC>")
  (evil-leader/set-key
    "e" 'eval-last-sexp
    "g" 'magit-status
    "r" 'restart-emacs
    "<SPC>" 'counsel-find-file
    "c" (defun open-config() (interactive) (find-file "~/.config/emacs/config.org")) 
    "<right>" 'next-buffer
    "<left>" 'previous-buffer
    "hf"  'describe-function
    "hv"  'describe-variable
    "hk" 'describe-key
    "k" 'kill-this-buffer
    "fd" 'init-file
    "ff" 'find-file
    "s" 'save-buffer
    "u" 'async-upgrade-packages
    ))

(use-package company
  :ensure t
  :config
  (progn
    (add-hook 'after-init-hook 'global-company-mode)))
(setq company-tooltip-align-annotations t)
(setq company-fontends '(company-pseudo-tooltip-frontend))
(setq company-tooltip-align-annotations t)
(setq company-minimum-prefix-length 2)

(use-package smartparens
  :ensure t)
(smartparens-global-mode)

(use-package rainbow-delimiters
  :ensure t)
(rainbow-delimiters-mode)
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)

(use-package counsel :ensure t)
(ivy-mode 1)
(counsel-mode)
(setq ivy-use-virtual-buffers t)
(setq enable-recursive-minibuffers t)
;; enable this if you want `swiper' to use it
;; (setq search-default-mode #'char-fold-to-regexp)
(global-set-key "\C-s" 'swiper)
(global-set-key (kbd "M-x") 'counsel-M-x)

(use-package magit :ensure t)
(setq magit-status-buffer-switch-function 'switch-to-buffer)

(use-package yasnippet :ensure t)
(use-package yasnippet-snippets :ensure t)
(yas-reload-all)
(add-hook 'prog-mode-hook #'yas-minor-mode)

(use-package flycheck :ensure t)
(add-hook 'after-init-hook #'global-flycheck-mode)

;; required by this package
(use-package nerd-icons :ensure t)
(use-package doom-modeline
  :ensure t
  :hook (after-init . doom-modeline-mode))
(setq doom-modeline-battery t)

(use-package tree-sitter-langs :ensure t)

(use-package async :ensure t)

(use-package popwin :ensure t)
(require 'popwin)
(popwin-mode 1)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(delete-selection-mode nil)
 '(package-selected-packages nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
