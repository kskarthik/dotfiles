;;; my-funcs.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 sk
;;
;; Author: sk <sk@earth>
;; Maintainer: sk <sk@earth>
;; Created: August 11, 2024
;; Modified: August 11, 2024
;; Version: 0.0.1
;; Keywords: Symbol’s value as variable is void: finder-known-keywords
;; Homepage: https://github.com/sk/my-funcs
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:



(provide 'my-funcs)
;;; my-funcs.el ends here
