#+TITLE: Karthik's Emacs Config
#+AUTHOR: Sai Karthik
#+STARTUP: showeverything
#+OPTIONS: toc:2 fold

This config is for `emacs-master` branch.

* Package Manager

I will stick with the default package manager
#+begin_src emacs-lisp
  ;;(add-to-list 'load-path "/path/to/installed-package-repo") ;; consider changing this in future
  ;; (load "my-funcs.el")
  ;; ( require 'my-funcs)
  (require 'package)
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
  (eval-and-compile
    (setq use-package-always-ensure t
	  use-package-expand-minimally t))
  (package-initialize)
#+end_src

* GUI Tweaks

** Disable Menubar, Toolbars and Scrollbars
#+begin_src emacs-lisp
  (menu-bar-mode -1)
  (tool-bar-mode -1)
  (scroll-bar-mode -1)
#+end_src

** Display Line Numbers and Truncated Lines
#+begin_src emacs-lisp
  (global-display-line-numbers-mode 1)
  (global-visual-line-mode t)
#+end_src

** Don't backup files
#+begin_src emacs-lisp
  (setq make-backup-files nil)
#+end_src
** Set font size
#+begin_src emacs-lisp
  (set-frame-font "JetbrainsMono 14" nil t)
  ;; (set-face-attribute 'default nil :height 160)
#+end_src

** Hide startup screen
#+begin_src emacs-lisp
  (setq inhibit-startup-screen t)
#+end_src

** Disable warnings buffer
we just need to be notified about errors
#+begin_src emacs-lisp
  (setq warning-minimum-level :error)
#+end_src

* Packages

** Colorscheme
#+begin_src emacs-lisp
  (use-package catppuccin-theme
    :ensure t)
  (load-theme 'catppuccin t nil)
#+end_src

** which key

helps with command suggestions

#+begin_src emacs-lisp
  (use-package which-key :ensure t)
  (require 'which-key)
  (which-key-mode)
#+end_src

** Evil Mode

Can't live without this 😝

#+begin_src emacs-lisp

  (defun async-upgrade-packages ()
    "update packages to latest version without blocking emacs"
    (interactive)
    (message "Upgrading packages in the background 🚀")
    (async-start (lambda ()
		   (package-refresh-contents)
		   (package-upgrade-all))
		 (lambda (_) (message "Successfully Upgraded all the packages! 😀"))))

  (use-package evil
    :ensure t
    :init      ;; tweak evil's configuration before loading it
    ;;(setq evil-want-keybinding nil)
    (setq evil-vsplit-window-right t)
    (setq evil-split-window-below t)
    (evil-mode))

  (use-package evil-leader
    :ensure t
    :config
    (global-evil-leader-mode t)
    (evil-leader/set-leader "<SPC>")
    (evil-leader/set-key
      "e" 'eval-last-sexp 
      "g" 'magit-status
      "r" 'restart-emacs
      "<SPC>" 'counsel-find-file
      "c" (defun open-config() (interactive) (find-file "~/.config/emacs/config.org")) 
      "<right>" 'next-buffer
      "<left>" 'previous-buffer
      "hf"  'describe-function
      "hv"  'describe-variable
      "hk" 'describe-key
      "k" 'kill-this-buffer
      "fd" 'init-file
      "ff" 'find-file
      "s" 'save-buffer
      "u" 'async-upgrade-packages
      ))
#+End_src

** Autocompletion

obviously, why not!

#+begin_src emacs-lisp
  (use-package company
    :ensure t
    :config
    (progn
      (add-hook 'after-init-hook 'global-company-mode)))
  (setq company-tooltip-align-annotations t)
  (setq company-fontends '(company-pseudo-tooltip-frontend))
  (setq company-tooltip-align-annotations t)
  (setq company-minimum-prefix-length 2)
#+end_src

** Smart parens

#+begin_src emacs-lisp
  (use-package smartparens
    :ensure t)
  (smartparens-global-mode)

  (use-package rainbow-delimiters
    :ensure t)
  (rainbow-delimiters-mode)
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
#+end_src

** Counsel

completion engine https://emacs-helm.github.io/helm/

#+begin_src emacs-lisp
  (use-package counsel :ensure t)
  (ivy-mode 1)
  (counsel-mode)
  (setq ivy-use-virtual-buffers t)
  (setq enable-recursive-minibuffers t)
  ;; enable this if you want `swiper' to use it
  ;; (setq search-default-mode #'char-fold-to-regexp)
  (global-set-key "\C-s" 'swiper)
  (global-set-key (kbd "M-x") 'counsel-M-x)
#+end_src

** Git

What's superior than magit !? 🙏 

#+begin_src emacs-lisp
  (use-package magit :ensure t)
  (setq magit-status-buffer-switch-function 'switch-to-buffer)
#+end_src

** Code Snippets

#+begin_src emacs-lisp
  (use-package yasnippet :ensure t)
  (use-package yasnippet-snippets :ensure t)
  (yas-reload-all)
  (add-hook 'prog-mode-hook #'yas-minor-mode)
#+end_src

**  Flycheck

#+begin_src emacs-lisp
  (use-package flycheck :ensure t)
  (add-hook 'after-init-hook #'global-flycheck-mode)
#+end_src

** Doom Modeline
#+begin_src emacs-lisp
  ;; required by this package
  (use-package nerd-icons :ensure t)
  (use-package doom-modeline
    :ensure t
    :hook (after-init . doom-modeline-mode))
  (setq doom-modeline-battery t)
#+end_src

** Treesitter languages
#+begin_src emacs-lisp
  (use-package tree-sitter-langs :ensure t)
#+end_src

** Async
A library which enables asynchronous programming
#+begin_src emacs-lisp
  (use-package async :ensure t)
#+end_src
** Popwin
#+begin_src emacs-lisp
  (use-package popwin :ensure t)
  (require 'popwin)
  (popwin-mode 1)
#+end_src
