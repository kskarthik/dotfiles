# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples
#
source ~/my/rahasyam.sh

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000
HISTFILESIZE=20000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
# my aliases
alias temacs='emacs -nw'
alias syu='sudo apt update && sudo apt upgrade && flatpak update'
alias ins='sudo apt install'
alias doom='~/.emacs.d/bin/doom'
alias gkapp='cd ~/my/work/gkapp && export NODE_OPTIONS=--openssl-legacy-provider && npm run serve'
alias gkcore='cd my/work/gkcore/ && gkenv && ./gkcore_cli.py serve'
alias v=nvim
alias vg='neovide --multigrid'
alias gkenv='. ~/virtualenvs/gkcore/bin/activate'
alias mpa='mpv --no-audio-display'
# git log format as markdown unordered list
alias gl='git log --format="- %h %s"'
alias dc='docker-compose'
alias nix-search='nix --extra-experimental-features "nix-command flakes" search nixpkgs'
# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etcbash_completion
  fi
fi


export MOZ_ENABLE_WAYLAND=1
export EDITOR=nvim

# for debian packaging setup
export DEBEMAIL="kskarthik@disroot.org"
export DEBFULLNAME="Sai Karthik"

# XDG paths
export XDG_DATA_DIRS=/usr/local/share:/usr/share:$XDG_DATA_DIRS
export XDG_CONFIG_DIRS=/etc:$XDG_CONFIG_DIRS
export XDG_SCREENSHOTS_DIR=$HOME/my/screenshots

# nvm
export NVM_DIR="$HOME/nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# guix config
# GUIX_PROFILE="/home/sk/.guix-profile"
# . "$GUIX_PROFILE"/etc/profile
# export GUIX_LOCPATH="$HOME/.guix-profile/lib/locale"

if [[ -d "$HOME"/go ]]; then
	export PATH=$HOME/go/bin:$PATH
fi

export PATH=~/.cargo/bin:$PATH
export PATH="$HOME"/my/bin:$PATH

eval 
            __main() {
                local major="${BASH_VERSINFO[0]}"
                local minor="${BASH_VERSINFO[1]}"

                if ((major > 4)) || { ((major == 4)) && ((minor >= 1)); }; then
                    source <(/usr/bin/starship init bash --print-full-init)
                else
                    source /dev/stdin <<<"$(/usr/local/bin/starship init bash --print-full-init)"
                fi
            }
            __main
            unset -f __main

 alias myip="curl -s ipinfo.io | jq"           
